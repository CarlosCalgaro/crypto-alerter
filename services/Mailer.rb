class Mailer 
    OPTIONS = { 
        :address              => "smtp.gmail.com",
        :port                 => 587,
        :user_name            => ENV['ACCOUNT_EMAIL'],
        :password             => ENV['ACCOUNT_PASSWORD'],
        :authentication       => 'plain',
        :enable_starttls_auto => true  
    }

    #TARGETS = ["ogioravi@gmail.com", "carloscalgarof@gmail.com", "rodrigoenunes@gmail.com"]
    TARGETS = ['carloscalgarof@gmail.com']
    def initialize()
        Mail.defaults do
            delivery_method :smtp, OPTIONS
        end
    end



    def send(hash)
        TARGETS.each do |e|
            Mail.deliver do
                from     OPTIONS[:user_name]
                to       e
                subject  'Relatório de CryptoMoedas'
                body      hash
                #add_file '/full/path/to/somefile.png'
            end    
        end
    
        puts "Mail delivered to #{TARGETS.to_s}" 
    end

end