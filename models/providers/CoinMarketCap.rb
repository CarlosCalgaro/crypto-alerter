require './models/Providers.rb'
class Providers::CoinMarketCap 
    @url = 'https://api.coinmarketcap.com/v1/ticker/' 
    attr_accessor :url


    def check
        response = Faraday.get 'https://api.coinmarketcap.com/v1/ticker/', 
                   options: { convert: "BRL", limit: 0}
        JSON.parse(response.body).each{|e| e.deep_symbolize_keys!} 
    end

end