class CreateCoinTable < ActiveRecord::Migration[5.1]
  def change
    create_table :coins do |t|
      t.string  :name
      t.string  :symbol
      t.integer :rank
      t.decimal :price
      t.decimal :price_to_btc
      t.string  :percent_change_1h
      t.string  :percent_change_24h
      t.string  :percent_change_7d
    end
  end
end
