require 'rubygems'
require 'bundler/setup'
Bundler.require()
#require 'nokogiri'

#require '/mnt/c/Workspace/CryptoCurrencyAlerter/HelloWorld.rb'
require './models/providers/CoinMarketCap.rb'
require './models/Coin.rb'
require './services/Mailer.rb'
ActiveRecord::Base.establish_connection(
    :adapter  => "postgresql",
    :host     => "localhost",
    :username => "postgres",
    :password => "postgres",
    :database => "crypto_d"
  )
    
  provider = Providers::CoinMarketCap.new
  mailer = Mailer.new

  puts "Server has started"
  while true 
    response =  provider.check
    #binding.pry
    
    response.each do |info|
        coin = Coin.create_with(symbol: info[:symbol], name: info[:name]).find_or_create_by(name: info[:name])    
        coin.update_attributes( 
            rank: info[:rank], 
            price: info[:price_usd], 
            price_to_btc: info[:price_btc],
            percent_change_1h: info[:percent_change_1h],
            percent_change_24h: info[:percent_change_24h],
            percent_change_7d: info[:percent_change_7d]    
        ) 

    end
   puts "Server fetched new information and updated the database at #{Time.now} ..."
   mailer = Mailer.new
   mailer.send(response[0])
   sleep(10)
  end
